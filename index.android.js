/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import IntelliSpendMain from './src/intelliSpendMain.js'

class intellispend extends Component {
  render() {
    return (
      <IntelliSpendMain />
    );
  }
}

AppRegistry.registerComponent('intellispend', () => intellispend);
