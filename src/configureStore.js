import {createStore, combineReducers, applyMiddleware} from 'redux';
import user from './auth/reducer/user.js'
import createSagaMiddleware from 'redux-saga'
import sagaMonitor from './sagaMonitor/sagaMonitor.js'
import nav from './app/reducer/nav.js';
import budget from './budget/reducer/budget.js';


const configureStore = () => {
    //combine all reducers
    let appReducer = combineReducers({
        user,
        nav,
        budget
    });

    //attach redux-saga middleware
    let sagaMiddleware = createSagaMiddleware({sagaMonitor});
    return {
        ... createStore(appReducer, applyMiddleware(sagaMiddleware)),
        runSaga: sagaMiddleware.run
    }
} 

export default configureStore;