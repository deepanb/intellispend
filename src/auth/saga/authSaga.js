//saga for auth
import {put, fork} from 'redux-saga/effects'
import {takeLatest} from 'redux-saga'
import * as actions from '../actions/actionCreators.js'
import Dal from './../../db/dal.js'
import hash from './../hash.js'

function* getUserInfo() {
  yield* takeLatest(
    'USER_INFO_REQUESTED',
    function* () {
      //fetch user info from database 
      let userInfo = Dal.User.getUserInfo();
      if (userInfo && userInfo.pin) {
          yield put(actions.userInfoReceived(userInfo.pin));
          return;
        }
      yield put(actions.failedToFetcUserInfo())
    })
}

function* createPin() {
  yield* takeLatest(
    'CREATE_PIN_REQUESTED',
    function* (action) {
      try {
        if (action.key) {
          let hashedKey = hash(action.key);
          Dal.User.writeUserInfo(hashedKey)
          yield put(actions.pinCreated(hashedKey));
        }
        else {
          yield put(actions.failedToCreatePin());
        }
      }
      catch (e) {
        yield put(actions.failedToCreatePin());
        console.log('ERROR creating pin: ' + e.message)
      }
    })
}

export default function* authSaga() {
  yield fork(getUserInfo);
  yield fork(createPin);
} 