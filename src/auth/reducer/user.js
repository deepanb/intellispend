/*
    user = {
            keyExists,
            isAuthenticated,
            isFetching,
            hashedKey,
            pinCreated
    }
*/
import hash from './../hash.js'

const user = (state = {}, action) => {
    switch (action.type) {
        
        case "USER_INFO_REQUESTED":
            return{
                ...state,
                keyExists:false,
                isAuthenticated: false,
                isFetching: true
            }

        case "USER_INFO_RECEIVED_SUCCESS":
            return {
                ...state,
                keyExists: true,
                isFetching: false,
                hashedKey:action.key
            }

        case "USER_INFO_RECEIVED_FAILURE":
            return {
                ...state,
                isFetching: false,
                keyExists: false
            }

        case "CREATE_PIN_SUCCESS":
            return {
                ...state,
                keyExists: true,
                hashedKey: action.hashedKey
            }

        case "CREATE_PIN_FAILURE":
            return {
                ...state,
                keyExists: false
            }

        case "AUTHENTICATE_USER":
        {
            let hashedUserInput = hash(action.key);
            return {
                ...state,
                isAuthenticated: (hashedUserInput == state.hashedKey)
            }
        }

        default:
            return state;

    }
}

export default user;