import jsSHA from 'jssha'

const hash = (key) => {
    let shaObj = new jsSHA("SHA-512", "TEXT");
    shaObj.update(key);
    return shaObj.getHash("HEX");
}

export default hash;