import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor: '#F5FCFF'},
  welcome: {fontSize: 20,textAlign: 'center',margin: 10},
  error: {textAlign: 'center',color: '#ff0000'},
  instructions: {textAlign: 'left',color: '#333333',marginBottom: 5},
  thumbnail: {width: 103,height: 151},
  label: {textAlign: 'left',color: '#333333',marginBottom: 5},
  loginContainer:{backgroundColor:'#eeeeee',margin:15,padding:50},
  button:{width:100,backgroundColor:'#222222',color:'#ffffff',height:30,padding:5,textAlign:'center',alignItems:'center'},
  topbar:{alignSelf: "stretch",height:100,backgroundColor: '#FDD7E4'},
  image:{alignItems:'center',justifyContent:'center',width:20,height:20}
});

export default styles;