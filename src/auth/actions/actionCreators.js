//Action creators for auth

export const requestUserInfo  = () => ({type: 'USER_INFO_REQUESTED'})

export const failedToFetcUserInfo  = () => ({type: 'USER_INFO_RECEIVED_FAILURE'})

export const userInfoReceived  = (key) => ({type: 'USER_INFO_RECEIVED_SUCCESS',key})

export const requestToCreatePin  = (key) => ({type: 'CREATE_PIN_REQUESTED',key})

export const failedToCreatePin  = (msg) => ({type: 'CREATE_PIN_FAILURE', msg})

export const pinCreated  = (hashedKey) => ({type: 'CREATE_PIN_SUCCESS', hashedKey})

export const authenticateUser = (key) => ({type: 'AUTHENTICATE_USER',key})