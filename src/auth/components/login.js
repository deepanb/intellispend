/***
  Login Component. Provide login screen and related functionality
***/
'use strict';
import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Text, View, StyleSheet, Image} from 'react-native';
import PinTextInput from './pinTextInput.js'
import {authenticateUser} from '../actions/actionCreators.js'
import {gotoScreen} from './../../app/actions/navActions.js';
import SlideInView from '../../common/components/slideInView.js'
import NumberPad from './numPad.js'
import {Divider, COLOR, TYPO} from 'react-native-material-design'

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { validating: false };
    this.errorMessage = '';
    this.message = 'Enter PIN to login'
  }

  componentWillUpdate(nextProps) {
    if (nextProps.isAuthenticated) {
      nextProps.gotoScreen('home');
    }
    else {
      this.errorMessage = 'Wrong PIN. Try again.'
      this.pin.clear();
    }
  }

  onPress(num) {
    console.log(`Number pressed : ${num}`);
    this.pin.setValueAt(num);
  }

  render() {
    const isError = (this.errorMessage !== '');
    const {theme} = this.props;
    return (
      <SlideInView>
        <View style={styles.loginContainer} >
          <View style={styles.logoContainer}>
            <Image style={styles.image} source={require('./../../images/intellispend.png')} />
            <Text style={[styles.banner, { color: COLOR[theme].color }]}>intellispend</Text>
            <Divider style={styles.divider} />
          </View>
          <Text style={isError ? styles.error : [styles.message, { color: COLOR.paperGrey900.color }]} >{isError ? this.errorMessage : this.message}</Text>
          <PinTextInput inputRef={(input) => this.pin = input} onInputComplete={(pin) => this.tryLogin(pin)} />
          <NumberPad onPress={this.onPress.bind(this)} />
        </View>
      </SlideInView>
    );
  }


  tryLogin(pin) {
    this.setState({ validating: false });
    if (pin) {
      this.props.authenticateUser(pin);
      this.setState({ validating: true });
    }
  }
}

const styles = StyleSheet.create({
  error: { textAlign: 'center', color: '#ff0000', ...TYPO.paperFontSubheading },
  message: { textAlign: 'center', ...TYPO.paperFontSubheading },
  logoContainer: { marginVertical: 20 },
  banner: { textAlign: 'center', ...TYPO.paperFontHeadline, fontWeight:'bold' },
  loginContainer: { flex: 1, justifyContent: 'center', alignItems:'center', backgroundColor: COLOR.paperGrey50.color },
  image: { width: 70, height: 70, margin: 0, alignSelf: "center" },
  divider: {borderColor:COLOR.paperYellowA700.color, borderWidth:0.5}
});

LoginScreen.propTypes = {
  authenticateUser: React.PropTypes.func.isRequired,
  isAuthenticated: React.PropTypes.bool.isRequired,
  theme: React.PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({ isAuthenticated: state.user.isAuthenticated, theme: state.nav.theme });
export default connect(mapStateToProps, { authenticateUser, gotoScreen })(LoginScreen);