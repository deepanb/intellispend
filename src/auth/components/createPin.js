/***
  Login Component. Provide login screen and related functionality
***/
'use strict';
import React, {Component} from 'react';
import {connect} from 'react-redux'
import { Text, View, StyleSheet, Image} from 'react-native';
import PinTextInput from './pinTextInput.js'
import {requestToCreatePin} from '../actions/actionCreators.js'
import SlideInView from '../../common/components/slideInView.js'
import {gotoScreen} from './../../app/actions/navActions.js'
import {Divider, COLOR, TYPO} from 'react-native-material-design'
import NumberPad from './numPad.js'

const PININPUT = { ONE: 1, TWO: 2 };

class CreatePinScreen extends Component {

  constructor(props) {
    super(props);
    this.errorMessage = '';
    this.message = 'Create PIN'
    this.isPinEntered = false;
    this.isPinConfirmed = false;

    this.state = {
      doesPinMatch: true,
      activePinInput: PININPUT.ONE
    }
  }

  componentWillReceiveProps(nextProps) {
    const {pinCreated, gotoScreen} = nextProps;
    pinCreated ? gotoScreen('login') : this.errorMessage = 'Failed to create PIN';
  }

  render() {
    const isError = (this.errorMessage !== '');
    const {theme} = this.props;
    const {activePinInput} = this.state;
    const pinOneStyle = activePinInput === PININPUT.ONE ? styles.active : styles.inactive;
    const pinTwoStyle = activePinInput === PININPUT.TWO ? styles.active : styles.inactive;
    console.log(`active input: ${JSON.stringify(activePinInput)}`)
    return (
      <SlideInView>
        <View style={styles.loginContainer} >
          <View style={styles.logoContainer}>
            <Image style={styles.image} source={require('./../../images/intellispend.png')} />
            <Text style={[styles.banner, { color: COLOR[theme].color }]}>intellispend</Text>
            <Divider style={styles.divider} />
          </View>
          <Text style={isError ? styles.error : [styles.message, { color: COLOR.paperGrey900.color }]} >{isError ? this.errorMessage : this.message}</Text>
          <View style={[pinOneStyle, {alignSelf :'stretch'}]}>
            <PinTextInput
              passedStyle={pinOneStyle}
              inputRef={(input) => this.pin = input}
              onInputComplete={(pin) => this.onPinEntered(pin)}
              onInputIncomplete={() => {this.isPinEntered = false; this.setState({activePinInput:PININPUT.ONE})}}
              />
          </View>
          <View style={[pinTwoStyle, {alignSelf :'stretch'}]}>
            <PinTextInput
              passedStyle={pinTwoStyle}
              inputRef={(input) => this.pinConfirm = input}
              onInputComplete={(pin) => this.onPinConfirmed(pin)}
              onInputIncomplete={() => {this.isPinConfirmed = false;  this.setState({activePinInput:PININPUT.TWO})}}
              />
          </View>
          <NumberPad onPress={this.onPress.bind(this)} />
        </View>
      </SlideInView>
    );
  }

  onPress(num) {
    console.log(`Number pressed : ${num}`);
    if (this.state.activePinInput === PININPUT.ONE) {
      this.pin.setValueAt(num);
    }
    else {
      this.pinConfirm.setValueAt(num);
    }
  }

  onPinEntered(pin) {
    this.isPinEntered = true;
    this.pinValue = pin;
    this.setState({ activePinInput: PININPUT.TWO });
    this.tryCreatePin()
  }

  onPinConfirmed(pinConfirm) {
    this.isPinConfirmed = true;
    this.pinConfirmValue = pinConfirm;
    this.setState({ activePinInput: PININPUT.ONE });
    this.tryCreatePin()
  }

  tryCreatePin() {
    if (this.isPinEntered && this.isPinConfirmed) {
      console.log(`pin1 = ${this.pinValue} , pin2=${this.pinConfirmValue}`)
      if (this.pinValue === this.pinConfirmValue) {
        this.setState({ doesPinMatch: true });
        this.props.requestToCreatePin(this.pinValue);
        this.errorMessage = '';
      }
      else {
        this.setState({ doesPinMatch: false });
        this.pin.clear();
        this.pinConfirm.clear();
        this.errorMessage = 'PIN does not match !!!';
      }
    }
  }
}

CreatePinScreen.propTypes = {
  requestToCreatePin: React.PropTypes.func.isRequired,
  pinCreated: React.PropTypes.bool.isRequired,
  theme: React.PropTypes.string.isRequired
}


const styles = StyleSheet.create({
  logoContainer: { marginVertical: 20 },
  loginContainer: { flex: 1, justifyContent: 'center', alignItems:'center', backgroundColor: COLOR.paperGrey50.color },
  image: { width: 70, height: 70, margin: 0, alignSelf: "center" },
  divider: { borderColor: COLOR.paperYellowA700.color, borderWidth:0.5 },
  active: { backgroundColor: COLOR.paperGreen100.color },
  inactive: { backgroundColor: COLOR.paperGrey50.color },
  error: { textAlign: 'center', color: '#ff0000', ...TYPO.paperFontSubheading, marginBottom:10 },
  message: { textAlign: 'center', ...TYPO.paperFontSubheading, marginBottom:10 },
  banner: { textAlign: 'center', ...TYPO.paperFontHeadline, fontWeight:'bold' },
});

const mapStateToProps = (state) => ({ pinCreated: state.user.keyExists, theme: state.nav.theme })
export default connect(mapStateToProps, { requestToCreatePin, gotoScreen })(CreatePinScreen);