/***
  Loading component. Loads necessary data for starting app
****/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SlideInView from '../../common/components/slideInView.js'
import {StyleSheet, Text, View}  from 'react-native';
import {requestUserInfo} from './../actions/actionCreators.js'
import {gotoScreen} from './../../app/actions/navActions.js'

class SplashScreen extends Component {

  componentDidMount() {
    this.props.requestUserInfo();
  }

  componentWillReceiveProps(nextProps) {

    const {startupState, gotoScreen} = nextProps;

    if (startupState === 'KEY_EXISTS') {
      //gotoScreen('home');
      gotoScreen('login');
    }
    else if (startupState === 'NO_KEY') {
      gotoScreen('pin');
    }
  }

  render() {
    return (
      <SlideInView>
        <View style={styles.container}>
          <Text style={styles.text}>
            Loading App..
          </Text>
        </View>
      </SlideInView>
    );
  }
}

SplashScreen.propTypes = {
  requestUserInfo: React.PropTypes.func.isRequired,
  startupState: React.PropTypes.string.isRequired
}

const getStartupState = ({ keyExists, isFetching }) => {

  let startupState = 'LOADING';
  if (isFetching !== undefined && keyExists !== undefined) {
    startupState = (isFetching === true) ? 'LOADING' : ((keyExists === true) ? 'KEY_EXISTS' : 'NO_KEY');
  }
  return startupState;
}


//connect the SplashScreen component to redux store
const mapStateToProps = (state) => ({ startupState: getStartupState(state.user) })
SplashScreen = connect(mapStateToProps, { requestUserInfo, gotoScreen })(SplashScreen);

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00ffaa'},
  text: {fontSize: 20, textAlign: 'center', margin: 10}
});

export default SplashScreen;