import React from 'react'
import {connect} from 'react-redux'
import {TouchableHighlight, View, StyleSheet, Text } from 'react-native'
import {COLOR} from 'react-native-material-design'

const NumberPad = ({onPress, theme}) => (
    <View style={styles.container}>
        {
            [[1, 2, 3], [4, 5, 6], [7, 8, 9]].map((row, index) => (
                <View key={index} style={styles.row}>
                    {
                        row.map((value) => (
                            <TouchableHighlight
                                key={value}
                                style={[styles.keyContainer, { borderColor: theme }]}
                                onPress={() => onPress(value)}
                                underlayColor={theme}>
                                <Text style={[styles.key, { color: theme }]}>{value}</Text>
                            </TouchableHighlight>
                        ))
                    }
                </View>
            ))
        }
    </View>
)

NumberPad.propTypes = {
    onPress: React.PropTypes.func.isRequired,
    theme: React.PropTypes.string.isRequired
}

const styles = StyleSheet.create({
    container: { padding: 10, justifyContent: 'center', marginVertical: 20, marginHorizontal: 40 },
    row: { flexDirection: 'row', alignItems: 'center'},
    keyContainer: { borderWidth: 1, margin: 10, borderRadius: 15, width: 50, height: 50, justifyContent: 'center' },
    key: { textAlign: 'center', textAlignVertical: 'center', fontSize: 20, fontWeight: 'bold' }
})

const mapStateToProps = (state) => ({ theme: COLOR[state.nav.theme].color });
export default connect(mapStateToProps)(NumberPad);