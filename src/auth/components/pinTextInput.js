import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import _ from 'lodash'
import {COLOR} from 'react-native-material-design'
import {connect} from 'react-redux'

class PinTextInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cursor: 0,
            value: ['', '', '', ''],
        };
    }

    clear() {
        this.setState({
            cursor: 0,
            value: ['', '', '', '']
        });
        this.props.onInputIncomplete && this.props.onInputIncomplete();
    }

    get value() {
        return _.join(this.state.value, '');
    }

    setValueAt(val, pos) {
        console.log(`.......... ${JSON.stringify(this.props)}`);
        let value = [...this.state.value];
        let {cursor} = this.state;
        if (pos && pos >= 0 && pos < 4) {
            cursor = (pos + 1) % 4;
            value[pos] = val;
        }
        else {
            value[cursor] = val;
            cursor = (cursor + 1) % 4;
        }

        this.setState({
            value,
            cursor
        });
        
        const {onInputComplete} = this.props;
        let emptyCell = value.filter(cell => cell === '');
        if (!emptyCell || emptyCell.length === 0) {
            onInputComplete(_.join(value, ''));
        }
    }

    onPress(index) {
        let value = [...this.state.value];
        value[index] = '';
        this.setState({
            cursor: index,
            value
        });
        this.props.onInputIncomplete && this.props.onInputIncomplete();
    }

    render() {
        let {cursor, value} = this.state;
        const {inputRef, theme, passedStyle} = this.props;
        let borderColorActive = { borderBottomColor: theme };
        let borderColorInactive = styles.inActive
        return (
            <View ref={() => inputRef(this)} style={[styles.container, passedStyle]}>
                {
                    value.map((val, index) => (
                        <TouchableOpacity
                            key={index}
                            style={[cursor == index ? borderColorActive : borderColorInactive, styles.textContainer]}
                            onPress={() => this.onPress(index)}
                            >
                            <Text style={[styles.text, { color: theme }]}>
                                {val !== '' ? '*' : ''}
                            </Text>
                        </TouchableOpacity>
                    ))
                }
            </View>
        )
    }
}

PinTextInput.propTypes = {
    theme: React.PropTypes.string.isRequired,
    inputRef: React.PropTypes.func.isRequired,
    onInputComplete: React.PropTypes.func.isRequired,
    onInputIncomplete: React.PropTypes.func,
    passedStyle: React.PropTypes.any
}

const styles = StyleSheet.create({
    container: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: COLOR.paperGrey50.color },
    inActive: { borderBottomColor: COLOR.paperGrey400.color },
    textContainer: { marginHorizontal: 5, marginVertical: 20, padding: 5, width: 40, height: 40, borderBottomWidth: 3, justifyContent: 'center' },
    text: { textAlign: 'center', textAlignVertical: 'center', fontSize: 20, fontWeight: 'bold' }
})

const mapStateToProps = (state) => ({ theme: COLOR[state.nav.theme].color });
export default connect(mapStateToProps)(PinTextInput);