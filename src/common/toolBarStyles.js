import { TYPO } from 'react-native-material-design';
const toolBarStyles = {
    toolBar: { position: 'absolute', top: 0, left: 0, right: 0, height: 56, flexDirection: 'row', alignItems: 'center', elevation: 4 },
    navBarTitle: { marginLeft: 72,...TYPO.paperFontTitle, color: 'rgba(255,255,255,.87)', textAlign: 'left' },
    toolBarLeftIcon: { marginLeft: 8 },
    toolBarIconColor: { color: 'rgba(255,255,255,.87)' },
    backButtonStyle: { height: 18, marginLeft: 10, tintColor: 'rgba(255,255,255,.87)' }
}

export default toolBarStyles;