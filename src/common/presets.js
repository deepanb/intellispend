import {COLOR} from 'react-native-material-design';

export const monthNames = [
    { month: 'JAN', color: COLOR.paperGreen400 },
    { month: 'FEB', color: COLOR.paperRed400 },
    { month: 'MAR', color: COLOR.paperPink400 },
    { month: 'APR', color: COLOR.paperPurple400 },
    { month: 'MAY', color: COLOR.paperDeepPurple400 },
    { month: 'JUN', color: COLOR.paperIndigo400 },
    { month: 'JUL', color: COLOR.paperBlue400 },
    { month: 'AUG', color: COLOR.paperLightBlue400 },
    { month: 'SEP', color: COLOR.paperCyan400 },
    { month: 'OCT', color: COLOR.paperTeal400 },
    { month: 'NOV', color: COLOR.paperLightGreen400 },
    { month: 'DEC', color: COLOR.paperLime400 }
];

export const categories = [
    { name: 'EMI', icon: 'emi', color: COLOR.paperRed400.color },
    { name: 'Education', icon: 'school', color: COLOR.paperPink400.color },
    { name: 'Household', icon: 'home', color: COLOR.paperPurple400.color },
    { name: 'Groceries', icon: 'shopping-basket', color: COLOR.paperDeepPurple400.color },
    { name: 'Office', icon: 'work', color: COLOR.paperGreen400.color },
    { name: 'Bills', icon: 'description', color: COLOR.paperIndigo400.color },
    { name: 'Internet', icon: 'settings-input-antenna', color: COLOR.paperBlue400.color },
    { name: 'Mobile', icon: 'smartphone', color: COLOR.paperLightBlue400.color },
    { name: 'Cable', icon: 'live-tv', color: COLOR.paperCyan400.color },
    { name: 'Savings', icon: 'savings', color: COLOR.paperTeal400.color },
    { name: 'Investment', icon: 'investment', color: COLOR.paperLightGreen700.color },
    { name: 'Travel', icon: 'flight', color: COLOR.paperLime700.color },
    { name: 'Restaurant', icon: 'dish-fork-spoon', color: COLOR.paperYellow900.color },
    { name: 'Credit Card', icon: 'credit-card', color: COLOR.paperAmber600.color },
    { name: 'Insurance', icon: 'insurance', color: COLOR.paperOrange400.color },
    { name: 'Electronics', icon: 'memory', color: COLOR.paperBrown400.color },
    { name: 'Movies', icon: 'movie', color: COLOR.paperDeepOrange400.color },
    { name: 'Fuel', icon: 'local-gas-station', color: COLOR.paperBlueGrey400.color },
    { name: 'Kids', icon: 'child-care', color: COLOR.paperGreen900.color }
];