import React, {Component} from 'react';
import {View} from 'react-native';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import fontelloConfig from './fontelloConfig.js';
import _ from 'lodash';

const CustomIcons = createIconSetFromFontello(fontelloConfig);
CustomIcons.glyphMap = {};
fontelloConfig.glyphs.forEach(glyph => {
    CustomIcons.glyphMap[glyph.css] = glyph.code;
});

const INTELLISPEND_ICON_SETS = _.map({
    MaterialIcons,
    CustomIcons
}, (component, name) => ({ name, component }));

export default class IntellispendIcon extends Component {

    getIconComponent(glyph) {
        let iconSets = INTELLISPEND_ICON_SETS.filter(iconSet => iconSet.component.glyphMap[glyph] !== undefined);
        return (iconSets ? iconSets[0].component : MaterialIcons)
    }

    getIconStyle({shape, backgroundColor, size}) {
        let style = { width: size, height: size, backgroundColor: backgroundColor, alignItems: 'center', justifyContent: 'center' };
        if (shape === 'round') {
            style.borderRadius = size / 2;
        }
        return style;
    }

    render() {
        const { name, size, color } = this.props;
        const Icon = this.getIconComponent(name);
        const style = this.getIconStyle(this.props);
        return (
            <View style={style}>
                <Icon name={name} color={color} size={0.6 * size} />
            </View>
        )
    }
}

IntellispendIcon.propTypes = {
    name: React.PropTypes.string.isRequired,
    size: React.PropTypes.number.isRequired,
    color: React.PropTypes.string.isRequired
}