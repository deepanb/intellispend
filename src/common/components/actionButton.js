import React from 'react'
import {TouchableHighlight, View} from 'react-native'
import { Avatar, COLOR } from 'react-native-material-design';

const ActionButton = ({icon, color, onPress}) => (
    <TouchableHighlight style={styles.actionButton} onPress={onPress}>
        <View>
            <Avatar size={40} icon={icon} {...COLOR.paperGrey50} backgroundColor={color} />
        </View>
    </TouchableHighlight>
);

ActionButton.propTypes = {
    icon: React.PropTypes.string.isRequired,
    color: React.PropTypes.string.isRequired,
    onPress: React.PropTypes.func
}

const styles = {
    actionButton: { elevation: 15, borderRadius: 50, }
}

export default ActionButton;