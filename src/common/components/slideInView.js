import React, {Component} from 'react'
import {Animated} from 'react-native'

export default class SlideInView extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            slideAnimation: new Animated.Value(0),
            outputRange : [-300, 0]
        };
    }

    componentDidMount() {
        this.setState(this.getOutputRange(this.props.slide));
        Animated.timing(this.state.slideAnimation, { toValue: 1 }).start();
    }

    componentWillReceiveProps(nextProps){
        this.setState(this.getOutputRange(nextProps.slide));
    }

    getOutputRange(slide){
        if(slide === 'rtl')
            return {outputRange: [300,0]}
        else
            return {outputRange: [-300,0]}
    }

    render() {
        return (
            <Animated.View
                style={{
                    flex:1,
                    opacity: this.state.slideAnimation,
                    transform: [{
                        translateX: this.state.slideAnimation.interpolate({
                            inputRange: [0, 1],
                            outputRange: this.state.outputRange
                        }),
                    }],
                }}
                >
                {this.props.children}
            </Animated.View>
        )
    }
}

SlideInView.propTypes = {
    children: React.PropTypes.element,
    style: React.PropTypes.object,
    slide: React.PropTypes.string
}