import React from 'react'
import {View, Modal, Text, ActivityIndicator} from 'react-native'
import { TYPO } from 'react-native-material-design';

const LoadingIndicator = ({loading, message, color}) => (
    <Modal visible={loading} onRequestClose={() => console.log('closed')} transparent={true} animationType='fade' >
        <View style={styles.container}>
            <ActivityIndicator size='large' animating={loading} color={color} />
            <Text style={[styles.message, { color }]}>{message}</Text>
        </View>
    </Modal>
)

const styles = {
    container: { position: 'absolute', flex: 1, top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center', backgroundColor: 'black', opacity: 0.8 },
    message: {...TYPO.paperFontCaption, margin: 10, fontSize:20 }
}

LoadingIndicator.propTypes = {
    loading: React.PropTypes.bool.isRequired,
    message: React.PropTypes.string,
    color: React.PropTypes.string,
}

export default LoadingIndicator;