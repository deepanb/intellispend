import React from 'react';
import {Image, View, StyleSheet} from 'react-native'

const SpendingScreen = () => (
    <View style={styles.container}>
       <Image source={require('./../../images/dance.gif')} /> 
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default SpendingScreen