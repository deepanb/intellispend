import React, {Component} from 'react'
import {View, ScrollView, Text, TouchableOpacity} from 'react-native'
import {COLOR, TYPO} from 'react-native-material-design';
import IntellispendIcon from '../../common/icons/intellispendIcon.js';
import {categories} from './../../common/presets.js';

const CategoryCard = ({name, icon, color, onSelected}) => (
    <TouchableOpacity style={[styles.cardContainer]} onPress={() => onSelected({ name, icon, color })}>
        <View style={styles.card}>
            <IntellispendIcon name={icon} color={COLOR.paperGrey50.color} backgroundColor={color} size={40} shape='round' />
            <Text style={styles.text}> {name} </Text>
        </View>
    </TouchableOpacity>
)

CategoryCard.propTypes = {
    name: React.PropTypes.string.isRequired,
    icon: React.PropTypes.string.isRequired,
    color: React.PropTypes.string.isRequired,
    onSelected: React.PropTypes.func.isRequired,
}

class ChooseCategory extends Component {

    onSelected(category) {
        this.props.gotoScreen('addBudget','Add', {type:'back', refresh : {category}});
    }

    transformTo2DArray(arr, noOfCols = 3) {
        let array2D = [];
        for (let i = 0; i < arr.length; i += noOfCols) {
            let row = [];
            array2D.push(row);
            for (let j = 0; j < noOfCols && (i + j) < arr.length; j++) {
                row.push(arr[i + j]);
            }
        }
        return array2D;
    }

    render() {
        let category2D = this.transformTo2DArray(categories);
        return (
            <ScrollView>
                {
                    category2D.map((row, i) => (
                        <View key={i} style={styles.row}>
                            {
                                row.map((category, j) => (
                                    <CategoryCard key={j} {...category} onSelected={this.onSelected.bind(this)} />
                                ))
                            }
                        </View>
                    ))
                }
            </ScrollView>
        );
    }
}

ChooseCategory.propTypes = {
    gotoScreen : React.PropTypes.func.isRequired
}

const styles = {
    row: { flex: 1, flexDirection: 'row', justifyContent: 'space-around' },
    cardContainer: { flex: 1 , margin: 10},
    card: { alignItems: 'center' },
    text: {...TYPO.paperFontCaption, textAlign: 'center', ...COLOR.paperGrey600 },
    line: { height: 1 },
    avatar: { borderRadius: 50 }
}

export default ChooseCategory;