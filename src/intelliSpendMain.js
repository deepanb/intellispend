'use strict';
import App from './app/components/app.js';
import React, {Component} from 'react' 
import {Provider} from 'react-redux'
import configureStore from './configureStore.js'
import rootSaga from './rootSaga.js'
import Dal from './db/dal.js'
import realmDal from './db/realm/realmDal.js'

export default class IntelliSpendMain extends Component {

    constructor(props){
        super(props);
        //configure store
        this.store = configureStore();
        this.store.runSaga(rootSaga);

        //configure database abstraction layer
        Dal.setDal(realmDal);
    }

    render() {
        return (
            <Provider store={this.store}>
                <App />
            </Provider>
        );
    }
}
