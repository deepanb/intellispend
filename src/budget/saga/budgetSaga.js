import {put, fork} from 'redux-saga/effects'
import {takeEvery, takeLatest} from 'redux-saga'
import Dal from './../../db/dal.js'
import * as actions from './../actions/budgetActions'

function* fetchBudgetLines() {
    yield takeLatest('FETCH_BUDGET_REQUESTED',
        function* () {
            try {
                let budgetlines = Dal.BudgetLine.fetchBudgetLines();
                yield put(actions.budgetFetched(budgetlines));
            }
            catch (e) {
                yield put(actions.failedToFetchBudget());
                console.log('ERROR fetching budget: ' + e.message)
            }
        })
}

function* saveBudgetLines() {
    yield takeEvery('SAVE_BUDGET_LINES_REQUESTED',
        function* ({id, month, description, category, amount, repeats, repeatId, mode}) {
            try {
                if (month && description && category && amount && repeats && repeatId && mode) {
                    let budgetLines = Dal.BudgetLine.saveBudgetLines({id, month, description, category, amount, repeats, repeatId, mode});
                    yield put(actions.budgetLinesSaved(budgetLines, mode));
                }
                else {
                    yield put(actions.failedToSaveBudget());
                }
            }
            catch (e) {
                yield put(actions.failedToSaveBudget());
                console.log('ERROR saving budgetlines: ' + e.message)
            }
        })
}

function* deleteBudgetLines() {
    yield takeEvery('DELETE_BUDGET_LINES_REQUESTED',
        function* ({id}) {
            try {
                if (id) {
                    let deletedIds = Dal.BudgetLine.deleteBudgetLines(id);
                    yield put(actions.budgetLinesDeleted(deletedIds));
                }
                else {
                    yield put(actions.failedToDeleteBudgetLiness());
                }
            }
            catch (e) {
                yield put(actions.failedToDeleteBudgetLiness());
                console.log('ERROR deleting budgetlines: ' + e.message)
            }
        })
}

export default function* budgetSaga() {
    yield fork(fetchBudgetLines);
    yield fork(saveBudgetLines);
    yield fork(deleteBudgetLines);
}