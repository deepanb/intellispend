export const saveBudgetRequested = ({id, category, month, description, amount, repeats, repeatId, mode}) => (
    {
        type: 'SAVE_BUDGET_LINES_REQUESTED',
        id,
        month,
        description, 
        category,
        amount,
        repeats,
        repeatId,
        mode
    }
);

export const deleteBudgetLinesRequested = (id) => (
    {
        type: 'DELETE_BUDGET_LINES_REQUESTED',
        id
    }
);
export const resetBudgetDeleteStatus = () => (
    {
        type: 'RESET_BUDGET_DELETE_STATUS'
    }
)

export const budgetLinesDeleted = (deletedIds) => (
    {
        type: 'DELETE_BUDGET_LINES_SUCCESS',
        deletedIds 
    }
);

export const failedToDeleteBudgetLiness = () => (
    {
        type: 'DELETE_BUDGET_LINES_FAILURE'
    }
);

export const resetBudgetSaveStatus = () => (
    {
        type: 'RESET_BUDGET_SAVE_STATUS'
    }
)

export const budgetLinesSaved = (budgetLines, mode) => (
    {
        type: 'SAVE_BUDGET_LINES_SUCCESS',
        budgetLines,
        mode 
    }
);

export const failedToSaveBudget = () => (
    {
        type: 'SAVE_BUDGET_LINES_FAILURE'
    }
);

export const fetchBudgetRequested = () => (
    {
        type: 'FETCH_BUDGET_REQUESTED'
    }
);

export const budgetFetched = (budgetLines) => (
    {
        type: 'FETCH_BUDGET_SUCCESS',
        budgetLines
    }
);

export const failedToFetchBudget = () => (
    {
        type: 'FETCH_BUDGET_FAILURE'
    }
);