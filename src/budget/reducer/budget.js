/*
state = {
    user,
    nav,
    budget : {
        saveStatus: 'saving', //'saving' OR 'success' OR 'failure' OR 'ignore'
        fetchStatus: 'fetching' //'fetching' OR 'success' OR 'failure'
        jan: [
            {id, category, amount, repeats, repeatId},
            {id, category, amount, repeats, repeatId},
            ....
        ],
        feb: [
            {id, category, amount, repeats, repeatId},
            {id, category, amount, repeats, repeatId},
            ....
        ],
        ...
        ...
        dec: [
            {id, category, amount, repeats, repeatId},
            {id, category, amount, repeats, repeatId},
            ....
        ]
    }
}
*/

import _ from 'lodash';
const budget = (
    state = { saveStatus: 'ignore', fetchStatus: 'fetching', deleteStatus: 'ignore', JAN: [], FEB: [], MAR: [], APR: [], MAY: [], JUN: [], JUL: [], AUG: [], SEP: [], OCT: [], NOV: [], DEC: [] },
    action) => {
    
    console.log(`budget reducer : action = ${JSON.stringify(action)}`)
    switch (action.type) {
        case 'RESET_BUDGET_SAVE_STATUS':
            return {
                ...state,
                saveStatus: 'ignore'
            }

            case 'SAVE_BUDGET_LINES_REQUESTED':
                return {
                    ...state,
                    saveStatus: 'saving'
                }
            
            case 'SAVE_BUDGET_LINES_SUCCESS':
                return {
                    ... getUpdatedBudget(state, action),
                    saveStatus: 'success'
                }

            case 'SAVE_BUDGET_LINES_FAILURE':
                return {
                    ...state,
                    saveStatus: 'failure',
                }

            case 'FETCH_BUDGET_REQUESTED':
                return {
                    ...state,
                    fetchStatus: 'fetching'
                }
            
            case 'FETCH_BUDGET_SUCCESS':
                return {
                    ...updateStateAfterAdd(state, action.budgetLines),
                    fetchStatus: 'success'
                }

            case 'FETCH_BUDGET_FAILURE':
                return {
                    ...state,
                    fetchStatus: 'failure'
                }
            
            case 'DELETE_BUDGET_LINES_REQUESTED':
                return {
                    ...state,
                    deleteStatus: 'deleting'
                }

            case 'DELETE_BUDGET_LINES_SUCCESS':
                return {
                    ...getBudgetOnDelete(state, action.deletedIds),
                    deleteStatus: 'success'
                }
            
            case 'DELETE_BUDGET_LINES_FAILURE':
                return {
                    ...state,
                deleteStatus: 'failure'
            }
            
            case 'RESET_BUDGET_DELETE_STATUS':
                return {
                    ...state,
                deleteStatus: 'ignore'
            }

            default:
                return state;
        }
    }

const getBudgetOnDelete = (state, deletedIds) => {
    if (!deletedIds || !deletedIds.length) {
        return state;
    }
    let newState = cloneState(state);

    deletedIds.forEach((itemToDelete) => {
        _.remove(newState[itemToDelete.month], item => item.id===itemToDelete.id)
    });

    return newState;
}

const getUpdatedBudget = (state, {budgetLines, mode}) => {
    
    if(!budgetLines){
        return state;
    }

    let newState = cloneState(state);

    if (mode === 'edit') {
        return updateStateAfterEdit(newState, budgetLines);
    }
    else {
        return updateStateAfterAdd(newState, budgetLines);
    }
}

const updateStateAfterEdit = (newState, budgetLines) => {

    //now update state
    budgetLines.added.forEach((line) => {
        newState[line.month].push(_.pick(line, ['id', 'description', 'category', 'amount', 'repeats', 'repeatId']));
    });
    
    budgetLines.deleted.forEach((itemToDelete) => {
        _.remove(newState[itemToDelete.month], item => item.id===itemToDelete.id);
    });

    budgetLines.edited.forEach((editedItem) => {
        let monthlyBudgetItems = newState[editedItem.month];
        _.remove(monthlyBudgetItems, item => item.id===editedItem.id);
        monthlyBudgetItems.push(_.pick(editedItem, ['id', 'description', 'category', 'amount', 'repeats', 'repeatId']));
    });

    return newState;
}

const updateStateAfterAdd = (newState, budgetLines) => {
    budgetLines.forEach((line) => {
        newState[line.month].push(_.pick(line, ['id', 'description', 'category', 'amount', 'repeats', 'repeatId']));
    });
    return newState;
}

const cloneState = (oldState) => {
    let newState = {};
    newState = {...oldState };
    //copy currentState to newState
    Object.keys(oldState).forEach(key => {
        if (oldState[key] instanceof Array) {
            newState[key] = [...oldState[key]];
        }
        else {
            newState[key] = oldState[key];
        }
    });

    return newState;
}

export default budget;