export const getMonthlyBudgetSummary = (monthlyBudget) => {
    let summary = {
        total: 0,
        remaining: 0,
        topSpend: 0,
    };

    monthlyBudget && monthlyBudget.forEach(({ amount, repeats }) => {
        summary.total += (repeats == 'Weekly')?amount*4:amount
    });

    return summary;
}