import React from 'react'
import {Text, TextInput, View, } from 'react-native';
import styles from './styles/addBudgetStyles.js'
import {COLOR} from 'react-native-material-design';
import IntellispendIcon from './../../common/icons/intellispendIcon.js';

const Description = ({theme, description, onChangeDescription}) => (
    <View style={[styles.container, styles.rowHeader]} >
        <IntellispendIcon name='description' size={styles.iconSize} color={COLOR[theme].color} />
        <View style={styles.rowBody} >
            <Text style={[styles.rowTextLabel, { color: COLOR[theme].color }]}>DESCRIPTION</Text>
            <TextInput
                value={description&&description}
                placeholder='What is it for?'
                underlineColorAndroid='transparent'
                placeholderTextColor={COLOR.paperGrey400.color}
                style={[styles.rowTextValue, styles.textMargin]}
                onChangeText={text => onChangeDescription(text)}
            />
        </View>
    </View>
);
Description.propTypes = {
    theme: React.PropTypes.string.isRequired,
    description: React.PropTypes.string,
    onChangeDescription: React.PropTypes.func.isRequired
}

export default Description;