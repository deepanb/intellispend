import React from 'react';
import {Text, View, TouchableHighlight} from 'react-native';
import styles from './styles/addBudgetStyles.js'
import {COLOR} from 'react-native-material-design';
import IntellispendIcon from './../../common/icons/intellispendIcon.js';

const Category = ({theme, onPress, category}) => (
    <TouchableHighlight onPress={onPress}>
        <View  style={[styles.container, styles.rowHeader]}>
            <IntellispendIcon name={category?category.icon:'view-list'} size={styles.iconSize} color={category?category.color:COLOR[theme].color} />
            <View style={styles.rowBody} >
                <Text style={[styles.rowTextLabel, { color: COLOR[theme].color }]}>CATEGORY</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={styles.rowTextValue}>{category?category.name:'Select a Category'}</Text>
                    <IntellispendIcon name='keyboard-arrow-right' size={20} color={COLOR.paperGrey600.color} />
                </View>
            </View>
        </View>
    </TouchableHighlight>
)
Category.propTypes = {
    theme: React.PropTypes.string.isRequired,
    onPress: React.PropTypes.func.isRequired,
    category: React.PropTypes.object,
}

export default Category;