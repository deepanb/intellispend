import React, {Component} from 'react'
import {connect} from 'react-redux';
import {ScrollView} from 'react-native'
import MonthCard from './monthCard.js';
import {gotoScreen} from './../../app/actions/navActions.js'
import SlideInView from './../../common/components/slideInView.js'
import {getMonthlyBudgetSummary} from './../selector/budgetSelector.js'
import {fetchBudgetRequested} from './../actions/budgetActions.js'
import {monthNames} from './../../common/presets.js'
import LoadingIndicator from './../../common/components/loadingIndicator.js'
import { COLOR } from 'react-native-material-design';

class BudgetScreen extends Component {

    componentDidMount() {
        if(this.props.budget.fetchStatus !== 'success'){
            this.props.fetchBudgetRequested();
        }
    }

    render() {
        //TODO: Add annual budget summary card at the top of this view
        const {budget} = this.props;
        return (
            <ScrollView>
                <LoadingIndicator loading={budget.fetchStatus == 'fetching'} message='Fetching...' color={COLOR.paperGreen200.color} />
                {
                    monthNames.map(
                        (month, i) => (
                            <SlideInView key={i} slide='rtl' >
                                <MonthCard  {...month} active={true} onPress={this.onPress.bind(this)} summary={getMonthlyBudgetSummary(budget && budget[month.month])} />
                            </SlideInView>
                        ))
                }
            </ScrollView>
        );
    }

    onPress(monthProps) {
        const { gotoScreen } = this.props;
        gotoScreen('planMonthlyBudget', 'Plan', monthProps);
    }
}

BudgetScreen.propTypes = {
    gotoScreen: React.PropTypes.func.isRequired,
    fetchBudgetRequested: React.PropTypes.func.isRequired,
    budget: React.PropTypes.object
}

const mapsStateToPtops = (state) => ({ budget: state.budget });
export default connect(mapsStateToPtops, { gotoScreen, fetchBudgetRequested })(BudgetScreen);