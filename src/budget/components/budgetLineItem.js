import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {categories} from './../../common/presets.js'
import IntellispendIcon from './../../common/icons/intellispendIcon.js';
import {TYPO, COLOR} from 'react-native-material-design';

const BudgetLineItem = ({id, description, category, amount, repeats, repeatId, highlightColor, onPressLineItem}) => {
    console.log(`budget line item : ${id}, ${description}, ${category}, ${amount}, ${repeats}, ${repeatId}`)
    const categoryDetails = categories.filter(c => (c.name === category))[0];
    const showRepeats = (repeats !== 'None')

    return (
        <TouchableOpacity style={[styles.lineItemContainer, { borderLeftColor: highlightColor }]} onPress={() => onPressLineItem({ id, description, category:categoryDetails, amount, repeats, repeatId })} >
            <View style={styles.left}>
                <IntellispendIcon name={categoryDetails.icon} color={COLOR.paperGrey50.color} backgroundColor={categoryDetails.color} size={36} shape='round' />
                <Text style={[styles.categoryText, { color: categoryDetails.color }]}>{categoryDetails.name}</Text>
            </View>
            <View style={styles.center}>
                <Text style={styles.descriptionText} ellipsizeMode='tail' numberOfLines={1} >{description}</Text>
                { 
                    showRepeats &&
                    <View style={[styles.repeats, {backgroundColor:highlightColor}]}>
                        <IntellispendIcon style={{margin:-20}} name='repeat' size={20} color={COLOR.paperGrey50.color} />
                        <Text style={[styles.repeatText, {color: COLOR.paperGrey50.color}]}>{repeats}</Text>
                    </View>
                }
            </View>
            <View style={styles.right}>
                <Text style={styles.amountText} ellipsizeMode='tail' numberOfLines={1} >{`₹ ${amount}`}</Text>
            </View>
        </TouchableOpacity>
    )
}

BudgetLineItem.propTypes = {
    id: React.PropTypes.string.isRequired,
    description: React.PropTypes.string.isRequired,
    category: React.PropTypes.string.isRequired,
    amount: React.PropTypes.number.isRequired,
    repeats: React.PropTypes.string.isRequired,
    repeatId: React.PropTypes.string.isRequired,
    highlightColor: React.PropTypes.string.isRequired,
    onPressLineItem: React.PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    lineItemContainer: { backgroundColor: '#ffffff', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: 2, marginHorizontal: 8, elevation: 2, height: 65, borderLeftWidth: 3  },
    left: { flex: 1, justifyContent: 'center', alignItems: 'center'},
    center: { flex: 3, justifyContent: 'center', padding: 5, margin:-3 },
    right: { flex: 1, justifyContent: 'center', alignItems: 'center', paddingRight: 5 },
    categoryText: {...TYPO.paperFontCaption, fontSize: 10, ...COLOR.paperGrey600, paddingTop: 3},
    amountText: {...TYPO.paperFontHeadline, textAlign: 'center', textAlignVertical : 'center', ...COLOR.paperGrey700, fontSize: 18 }, 
    descriptionText: {...TYPO.paperFontSubheading, ...COLOR.paperGrey600, fontSize: 16, paddingLeft: 3, paddingTop:5},
    repeatText: {...TYPO.paperFontCaption, ...COLOR.paperGrey50, fontSize: 10, margin:-3, paddingRight:8 },
    repeats: { flexDirection: 'row', marginVertical: 5, marginHorizontal: 3, borderRadius:20, width:70, justifyContent: 'center', alignItems: 'center'} 
});

export default BudgetLineItem;