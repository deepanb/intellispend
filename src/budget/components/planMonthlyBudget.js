import React, {Component} from 'react';
import MonthCard from './monthCard.js';
import {View, ScrollView} from 'react-native'
import ActionButton from '../../common/components/actionButton.js'
import {connect} from 'react-redux';
import {gotoScreen} from './../../app/actions/navActions.js'
import {getMonthlyBudgetSummary} from './../selector/budgetSelector.js'
import BudgetLineItem from './budgetLineItem.js'
import { resetBudgetSaveStatus, resetBudgetDeleteStatus } from './../actions/budgetActions.js'

class PlanMonthlyBudgetScreen extends Component {

    componentDidUpdate (){
        //reset save, delete status
        const {resetBudgetSaveStatus, resetBudgetDeleteStatus} = this.props;
        resetBudgetSaveStatus();
        resetBudgetDeleteStatus();
    }

    render() {
        const {month, color, gotoScreen, budget} = this.props;
        
        return (
            <View style={styles.container} >
                <MonthCard style={styles.monthCard} month={month} color={color} active={false} summary={getMonthlyBudgetSummary(budget)} />
                <ScrollView>
                    {
                        budget && budget.map((lineItem, index) => (
                            <BudgetLineItem key={index} {...lineItem} highlightColor={color.color} onPressLineItem={this.onPressLineItem.bind(this)} />
                        ))
                    }
                </ScrollView>
                <View style={styles.buttonContainer} >
                    <ActionButton icon='add' color={color.color} onPress={() => gotoScreen('addBudget', 'Add', { month, color, mode: 'add' })} />
                </View>
            </View>
        );
    }

    onPressLineItem({ id, description, category, amount, repeats, repeatId }) {
        console.log(`plan monthly budget: ${id}, ${description}, ${JSON.stringify(category)}, ${amount}, ${repeats}, ${repeatId}`)
        const {month, color} = this.props;
        gotoScreen('addBudget', 'Edit', { title:'Edit', month, color, id, description, category, amount, repeats, repeatId, mode: 'edit' });
    }

}

const styles = {
    container: { flex: 1 },
    monthCard: { position: 'absolute', top: 0, left: 0 },
    buttonContainer: { position: 'absolute', bottom: 25, right: 25 },
};

PlanMonthlyBudgetScreen.propTypes = {
    month: React.PropTypes.string.isRequired,
    color: React.PropTypes.object.isRequired,
    gotoScreen: React.PropTypes.func.isRequired,
    resetBudgetSaveStatus: React.PropTypes.func.isRequired,
    resetBudgetDeleteStatus: React.PropTypes.func.isRequired,
    budget: React.PropTypes.array //TODO: see if we can use React.Shape validation
}

const mapStateToProps = (state, ownProps) => ({ budget: state.budget[ownProps.month] });
export default connect(mapStateToProps, { gotoScreen, resetBudgetSaveStatus, resetBudgetDeleteStatus })(PlanMonthlyBudgetScreen);