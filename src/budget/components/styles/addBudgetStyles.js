import {COLOR, TYPO} from 'react-native-material-design';
const addBudgetStyles = {
    parentContainer: {flex:1},
    banner: { height: 140, justifyContent: 'center', padding: 20 },
    heading: {...TYPO.paperFontSubheading, color: COLOR.paperGrey50.color },
    amount: {...TYPO.paperFontHeadline, color: COLOR.paperGrey50.color },
    amountContainer: {position: 'absolute', top: 0, left:0},
    container: { flexDirection: 'row', alignItems: 'center' },
    rowHeader: { paddingHorizontal: 20, paddingVertical: 10, },
    rowBody: { paddingHorizontal: 20, paddingVertical: 10, justifyContent: 'center', flex: 1 },
    rowTextLabel: {...TYPO.paperFontCaption, fontSize: 10, textAlignVertical: 'center' },
    rowTextValue: {...TYPO.paperFontSubheading, textAlignVertical: 'center', color: COLOR.paperGrey600.color },
    divider: { marginHorizontal: 20 },
    picker: { marginLeft: -9, marginVertical: -15 },
    iconSize: 36,
    textMargin: {marginLeft:-5, marginTop:-10, marginBottom:-20},
    navBarButton: { marginTop: -8}
}

export default addBudgetStyles;