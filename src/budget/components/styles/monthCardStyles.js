import {TYPO, COLOR} from 'react-native-material-design';

const col = { flex: 1, justifyContent: 'space-around', alignItems: 'center' };

const monthCardStyles = (primary, active = true) => (
    {
        card: { backgroundColor: '#ffffff', borderRadius: 3, margin: 8, elevation: 4, flexDirection: 'row', height: active ? 140 : 90, justifyContent: 'space-around' },
        left: {...col, backgroundColor: primary.color, borderTopLeftRadius: 3, borderBottomLeftRadius: 3 },
        mid: {...col, flex: 2 },
        right: {...col, borderTopRightRadius: 3, borderBottomRightRadius: 3, justifyContent: 'flex-end', paddingBottom: 15 },
        month: [TYPO.paperFontTitle, COLOR.paperGrey50, { flex: 1 }],
        graph: { size: 40, color: COLOR.paperGrey50.color },
        graphBorder: { borderColor: COLOR.paperGrey50.color, borderWidth: 2, borderRadius: 2 },
        miniCardContainer: { flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', flex: 1 },
        miniCard: { backgroundColor: '#ffffff', justifyContent: 'center', flex: 1, margin: 5 },
        rowText: {...TYPO.paperFontCaption, textAlign: 'center', textAlignVertical: 'center', fontSize: 10, ...COLOR.paperGrey600 },
        largeText: {...TYPO.paperFontHeadline, textAlign: 'center', textAlignVertical: 'center', ...COLOR.paperGrey900 },
        smallText: {...TYPO.paperFontSubheading, textAlign: 'center', textAlignVertical: 'center', ...COLOR.paperGrey900 }
    }
);

export default monthCardStyles;