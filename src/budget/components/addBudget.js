import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Alert, View, ScrollView, TouchableOpacity, ToastAndroid } from 'react-native'
import LoadingIndicator from './../../common/components/loadingIndicator.js'
import { Divider, COLOR } from 'react-native-material-design';
import IntellispendIcon from './../../common/icons/intellispendIcon.js';
import {NavBar} from 'react-native-router-flux'
import Event from './../../common/events.js'
import toolBarStyles from './../../common/toolBarStyles.js'
import {gotoScreen} from './../../app/actions/navActions.js'
import {saveBudgetRequested, deleteBudgetLinesRequested } from './../actions/budgetActions.js'
import Repeats from './repeats.js';
import Category from './category.js';
import Amount from './amount.js';
import Month from './month.js';
import Description from './description.js';
import styles from './styles/addBudgetStyles.js'

const event = new Event();
class AddBudget extends Component {

    constructor(props) {
        super(props);
        this.repeatFrequency = 'None'
        this.state = {
            id: props.id || undefined,
            category: props.category || undefined,
            description: props.description || '',
            month: props.month,
            color: props.color,
            amount: props.amount || 0,
            repeats: props.repeats || 'None',
            mode: props.mode || 'add',
            repeatId: props.repeatId || '-1'
        };
        event.removeAllListeners();
        event.on('save-clicked', this.save.bind(this));
        event.on('delete-clicked', this.seekDeleteConfirmation.bind(this));
    }

    save(mode) {
        console.log('save started');
        const {category, description, amount} = this.state
        if(category && amount > 0 && description.length>0){
            this.props.saveBudgetRequested({
                ...this.state,
                category: this.state.category.name,
                repeats: this.repeatFrequency.value,
                mode
            });
        }
        else{
            console.log('Description ' + description);
            let msg = `Please enter values for${amount <= 0?' Amount,':''}${(!description.length)?' Description,':''}${!category?' Category':''}`
            ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.BOTTOM);
        }
    }

    seekDeleteConfirmation() {
        Alert.alert(
            'Confirm',
            'This action cannot be reversed. Do you want to delete?',
            [
                {text: 'No'},
                {text: 'Delete', onPress:this.deleteBudgetLine.bind(this)}
            ]);
    }

    deleteBudgetLine(){
        console.log('Deleting ....');
        const {deleteBudgetLinesRequested, gotoScreen, month, color} = this.props;
        deleteBudgetLinesRequested(this.state.id);
        //goback to previous screen irrespective of success/failure
        gotoScreen('planMonthlyBudget', 'Plan', { type: 'back', refresh: { month, color } });
    }

    onCategoryPress() {
        const {gotoScreen} = this.props;
        gotoScreen('category', 'Choose Category', { gotoScreen });
    }

    componentWillReceiveProps(nextProps) {
        const {saveStatus, gotoScreen, category} = nextProps;
        category && this.setState({ category });
        const {month, color} = this.state;
        console.log(`addBudget.componentWillReceiveProps: saveStatus -> new=${saveStatus} old=${this.props.saveStatus}`)
        if (saveStatus === 'success') {
            event.removeAllListeners();
            gotoScreen('planMonthlyBudget', 'Plan', { type: 'back', refresh: { month, color } });
        }
    }

    componentWillUnmount() {
        event.removeAllListeners();
    }

    render() {
        const {theme} = this.props;
        const {description, category, repeats, amount, month} = this.state;
        console.log(`add budget: ${JSON.stringify(this.state)}`)
        const saveStatus = this.props.saveStatus;
        //TODO: what to show when saveStatus=failure .. that is saving budget line has failed 
        return (
            <View style={styles.parentContainer}>
                <Amount style={styles.amountContainer} theme={theme} amount={amount} onChangeAmount={amount => this.setState({ amount })} />
                <ScrollView>
                    <LoadingIndicator loading={saveStatus == 'saving'} message='Saving...' color={COLOR.paperGreen200.color} />
                    <Description theme={theme} description={description} onChangeDescription={(description) => this.setState({ description })} />
                    <Divider style={styles.divider} />
                    <Category category={category} theme={theme} onPress={this.onCategoryPress.bind(this)} />
                    <Divider style={styles.divider} />
                    <Month theme={theme} month={month} onMonthSelected={(month, color) => this.setState({ month, color })} />
                    <Divider style={styles.divider} />
                    <Repeats theme={theme} repeats={repeats} ref={c => (this.repeatFrequency = c)} />
                    <Divider style={[styles.divider, {marginBottom: 15}]} />
                </ScrollView>
            </View>
        )
    }

    static renderRightButton(props) {
        return (
            <View style={{flexDirection: 'row'}}>
                {
                    (props.mode === 'edit') &&
                    <TouchableOpacity  style={styles.navBarButton} color={toolBarStyles.toolBarIconColor.color} onPress={() => { event.emit('delete-clicked') }}>
                        <IntellispendIcon name='delete' size={40} color={toolBarStyles.toolBarIconColor.color} />
                    </TouchableOpacity>
                }
                <TouchableOpacity  style={styles.navBarButton} color={toolBarStyles.toolBarIconColor.color} onPress={() => { event.emit('save-clicked', props.mode) }}>
                    <IntellispendIcon name='done' size={40} color={toolBarStyles.toolBarIconColor.color} />
                </TouchableOpacity>
            </View>
        );
    }

    static renderNavigationBar(props) {
        return (
            <NavBar {...props} renderRightButton={AddBudget.renderRightButton} />
        );
    }
}

AddBudget.propTypes = {
    theme: React.PropTypes.string.isRequired,
    month: React.PropTypes.string.isRequired,
    color: React.PropTypes.object.isRequired,
    gotoScreen: React.PropTypes.func.isRequired,
    mode: React.PropTypes.oneOf(['add', 'edit']),
    saveBudgetRequested: React.PropTypes.func.isRequired,
    deleteBudgetLinesRequested: React.PropTypes.func.isRequired,
    id: React.PropTypes.string,
    amount: React.PropTypes.number,
    repeats: React.PropTypes.oneOf(['None', 'Weekly', 'Monthly']),
    repeatId: React.PropTypes.string,
    saveStatus: React.PropTypes.oneOf(['ignore', 'saving', 'success', 'failure']),
    deleteStatus: React.PropTypes.oneOf(['ignore', 'deleting', 'success', 'failure']),
    category: React.PropTypes.object,
    description: React.PropTypes.string,
}

const mapStateToProps = (state) => ({ theme: state.nav.theme, saveStatus: state.budget.saveStatus });
export default connect(mapStateToProps, { gotoScreen, saveBudgetRequested, deleteBudgetLinesRequested })(AddBudget);