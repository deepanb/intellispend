import React, {Component} from 'react';
import {View, Text, Animated} from 'react-native';
import styles from './styles/addBudgetStyles';
import { COLOR, IconToggle, RadioButtonGroup } from 'react-native-material-design';
import IntellispendIcon from './../../common/icons/intellispendIcon.js';

export default class Repeats extends Component {

    constructor(props) {
        super(props);
        const {repeats} = props;
        this.items = [{ value: 1, label: 'None' }, { value: 2, label: 'Weekly' }, { value: 3, label: 'Monthly' }];
        this.state = {
            isChecked: repeats ? (repeats !== 'None') : false,
            animatedHeight: new Animated.Value(),
            minHeight: null,
            maxHeight: null,
            selected: repeats ? (this.items.filter(item => item.label === repeats)[0].value) : 1
        };
    }

    componentDidUpdate() {
        const {isChecked, minHeight, maxHeight, animatedHeight} = this.state;
        let initialValue = isChecked ? minHeight : maxHeight;
        let finalValue = isChecked ? maxHeight : minHeight;
        //start the animation
        animatedHeight.setValue(initialValue);
        Animated.spring(
            animatedHeight,
            {
                toValue: finalValue,
            }
        ).start();
    }

    render() {
        const {theme} = this.props;
        const {isChecked, selected} = this.state

        return (
            <View style={[styles.rowBody]}>
                <View style={styles.container}>
                    <IconToggle color={COLOR[theme].color} onPress={this.onRepeatCheckboxPressed.bind(this)} >
                        <IntellispendIcon
                            name={isChecked ? 'check-box' : 'check-box-outline-blank'}
                            size={styles.iconSize}
                            color={COLOR[theme].color}
                            />
                    </IconToggle>
                    <Text style={[styles.rowTextLabel, { color: COLOR[theme].color, paddingHorizontal: 20 }]}>REPEATS</Text>
                </View>
                <Animated.View
                    style={{ paddingLeft: 23, paddingVertical: -12, height: this.state.animatedHeight }}
                    onLayout={this.setHeightRange.bind(this)}
                    >
                    <RadioButtonGroup
                        ref='repeatFrequency'
                        primary={'paperGreen'}
                        selected={selected}
                        items={this.getVisibleButtons(isChecked)}
                        />
                </Animated.View>
            </View>
        );
    }

    onRepeatCheckboxPressed() {
        let isChecked = !this.state.isChecked;
        if (!isChecked) {
            this.refs.repeatFrequency.value = 1;
        }
        this.setState({ isChecked });
    }

    getVisibleButtons(isChecked) {
        let visibleButtons = this.items;
        if (!isChecked) {
            visibleButtons = [{...this.items[0], disabled: true}];
    }
        return visibleButtons;
    }

get value() {
    const {repeatFrequency} = this.refs;
    if (this.state.isChecked) {
        return this.items.filter(item => (repeatFrequency.value === item.value))[0].label;
    }
    else {
        return this.items[0].label;
    }
}

setHeightRange({ nativeEvent }) {
    const {isChecked, minHeight, maxHeight} = this.state;
    if (minHeight !== null || maxHeight !== null) {
        return;
    }
    let h = nativeEvent.layout.height;
    if (isChecked) {
        this.setState({
            minHeight: h / 5,
            maxHeight: h
        });
    }
    else {
        this.setState({
            minHeight: h,
            maxHeight: h * 5
        });
    }

}
}

Repeats.propTypes = {
    theme: React.PropTypes.string.isRequired,
    repeats: React.PropTypes.string
}