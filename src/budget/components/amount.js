import React from 'react'
import {Text, TextInput, View, } from 'react-native';
import styles from './styles/addBudgetStyles.js'
import {COLOR} from 'react-native-material-design';

const Amount = ({theme, amount, onChangeAmount}) => (
    <View style={[styles.banner, { backgroundColor: COLOR[theme].color }]} >
        <Text style={styles.heading}>BUDGETED AMOUNT</Text>
        <View style={styles.container}>
            <Text style={styles.amount}>₹</Text>
            <TextInput
                value={amount?`${amount}`:''}
                placeholder='0'
                placeholderTextColor={COLOR.paperGrey400.color}
                keyboardType='numeric'
                style={[styles.amount, { width: 140 }]}
                underlineColorAndroid={COLOR.paperGrey50.color}
                onChangeText={amount => onChangeAmount(Number(amount))}
                />
        </View>
    </View>
);
Amount.propTypes = {
    theme: React.PropTypes.string.isRequired,
    amount: React.PropTypes.number,
    onChangeAmount: React.PropTypes.func.isRequired
}

export default Amount;