import React from 'react';
import {Text, View, Picker } from 'react-native';
import styles from './styles/addBudgetStyles.js'
import {COLOR} from 'react-native-material-design';
import IntellispendIcon from './../../common/icons/intellispendIcon.js';
import {monthNames} from './../../common/presets.js'

const Month = ({theme, month, onMonthSelected}) => (
    <View  style={[styles.container, styles.rowHeader]}>
        <IntellispendIcon name='date-range' size={styles.iconSize} color={COLOR[theme].color} />
        <View style={styles.rowBody} >
            <Text style={[styles.rowTextLabel, { color: COLOR[theme].color }]}>MONTH</Text>
            <Picker
                selectedValue={month}
                onValueChange={(m) => onMonthSelected(m, monthNames.filter(month => (m===month.month))[0].color)}
                style={styles.picker}
                mode='dropdown'>
                {
                    monthNames.map(
                        (entry, i) => (
                            <Picker.Item style={{ fontSize: 5 }} key={i} label={entry.month} color={entry.color.color} value={entry.month} />
                        ))
                }
            </Picker>
        </View>
    </View>
)
Month.propTypes = {
    theme: React.PropTypes.string.isRequired,
    month: React.PropTypes.string.isRequired,
    onMonthSelected: React.PropTypes.func.isRequired
}

export default Month;