import React from 'react'
import {Text, View} from 'react-native';
import monthCardStyles from './styles/monthCardStyles.js'
import {Icon} from 'react-native-material-design';
import ActionButton from '../../common/components/actionButton.js'

const MonthCard = ({month, color, active, onPress, summary}) => {

    const styles = monthCardStyles(color, active);
    const {total, remaining, topSpend} = {
        total: `₹ ${summary.total || '0'}`,
        remaining: `₹ ${summary.total || '0'}`,
        topSpend: `₹ ${summary.topSpend || '0'}`
    }

    const renderActiveView = () => (
        <View style={styles.card}>
            <View style={styles.left}>
                <View><Text style={styles.month} >{month}</Text></View>
                <View style={styles.graphBorder} >
                    <Icon name='trending-up' {...styles.graph} />
                </View>
            </View>
            <View style={styles.mid}>
                <View style={styles.miniCard}>
                    <Text style={styles.rowText}>REMAINING</Text>
                    <Text style={styles.largeText} ellipsizeMode='tail' numberOfLines={1}>{remaining}</Text>
                </View>
                <View style={styles.miniCardContainer}>
                    <View style={styles.miniCard}>
                        <Text style={[styles.rowText]}>TOP SPEND</Text>
                        <Text style={[styles.smallText]} ellipsizeMode='tail' numberOfLines={1}>{topSpend}</Text>
                    </View>
                    <View style={styles.miniCard}>
                        <Text style={[styles.rowText]}>BUDGET</Text>
                        <Text style={[styles.smallText]} ellipsizeMode='tail' numberOfLines={1}>{total}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.right}>
                <ActionButton icon='edit' color={color.color} onPress={() => onPress({ month, color })} />
            </View>
        </View>
    );

    const renderSummaryView = () => (
        <View style={styles.card}>
            <View style={styles.left}>
                <View><Text style={styles.month} >{month}</Text></View>
            </View>
            <View style={styles.mid}>
                <View style={styles.miniCard}>
                    <Text style={styles.rowText}>BUDGET</Text>
                    <Text style={styles.largeText} ellipsizeMode='tail' numberOfLines={1}>{total}</Text>
                </View>
                <View style={styles.miniCardContainer}>
                    <View style={styles.miniCard}>
                        <Text style={[styles.rowText]}>REMAINING</Text>
                        <Text style={[styles.smallText]} ellipsizeMode='tail' numberOfLines={1}>{remaining}</Text>
                    </View>
                    <View style={styles.miniCard}>
                        <Text style={[styles.rowText]}>TOP SPEND</Text>
                        <Text style={[styles.smallText]} ellipsizeMode='tail' numberOfLines={1}>{topSpend}</Text>
                    </View>
                </View>
            </View>
        </View>
    );

    if (active) {
        return renderActiveView();
    }
    else {
        return renderSummaryView();
    }
}

MonthCard.propTypes = {
    month: React.PropTypes.string.isRequired,
    color: React.PropTypes.object.isRequired,
    active: React.PropTypes.bool.isRequired,
    onPress: React.PropTypes.func,
    summary: React.PropTypes.object
}

export default MonthCard;