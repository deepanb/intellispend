//aggregate all the sagas
import { fork } from 'redux-saga/effects'
import authSaga from './auth/saga/authSaga.js'
import budgetSaga from './budget/saga/budgetSaga.js'

export default function* rootSaga(){
    try{
        yield fork(authSaga);
        yield fork(budgetSaga);
    }
    catch(e){
        console.log("rootSaga.js: " + e.message);
        console.log("rootSaga.js: " + e.stack);
    }
}