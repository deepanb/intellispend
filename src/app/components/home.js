/***
  Welcome Component. This would be post login page
***/
'use strict';
import React, { Component } from 'react';
import {connect} from 'react-redux'
import { DefaultRenderer} from 'react-native-router-flux';
import DrawerMenu from './drawerMenu.js';
import Layout from './layout.js'
import {changeDrawerState} from './../actions/navActions.js'

class Home extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { navigationState, open, onNavigate, changeDrawerState } = this.props;
    if (!navigationState.children) {
      return null;
    }
    const children = navigationState.children;
    return (
      <Layout
        drawerOpen={open || false}
        width={300}
        onDrawerClose={()=>changeDrawerState(false)}
        onDrawerOpen={()=>changeDrawerState(true)}
        renderSideMenu={() => <DrawerMenu />}
        >
        <DefaultRenderer navigationState={children[0]} onNavigate={onNavigate} />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({open:state.nav.open});
export default connect(mapStateToProps, {changeDrawerState})(Home);

Home.propTypes = {
  navigationState: React.PropTypes.object,
  onNavigate: React.PropTypes.func,
  open: React.PropTypes.bool,
  changeDrawerState: React.PropTypes.func
}
