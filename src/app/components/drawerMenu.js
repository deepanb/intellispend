import React from 'react'
import {View, Image, StyleSheet} from 'react-native';
import {Drawer, Avatar} from 'react-native-material-design'
import {connect} from 'react-redux';
import {gotoScreen, changeDrawerState} from './../actions/navActions.js'

const DrawerMenu = ({currentScreen, gotoScreen, changeDrawerState}) => {

    const go = (screenKey, title) => {
        gotoScreen(screenKey, title);
        changeDrawerState(false);
    }

    return (
        <Drawer theme='light'>
            <Drawer.Header image={<Image source={require('./../../images/budget-green.jpeg')} />}>
                <View style={styles.container}>
                    <Avatar size={80} /*image={<Image source={require('./../images/intellispend.png')} />}*/ />
                </View>
            </Drawer.Header>

            <Drawer.Section
                items={[
                    {
                        icon: 'attach-money',
                        value: 'Budget',
                        active: currentScreen == 'budget',
                        onPress: () => { go('budget', 'Budget') },
                        onLongPress: () => { go('budget', 'Budget') }
                    },
                    {
                        icon: 'compare-arrows',
                        value: 'Spendings',
                        active: currentScreen == 'spends',
                        onPress: () => { go('spends', 'Spendings') },
                        onLongPress: () => { go('spends', 'Spendings') }
                    }]}
                />
        </Drawer>
    )

}

DrawerMenu.propTypes = {
    currentScreen: React.PropTypes.string.isRequired,
    gotoScreen: React.PropTypes.func.isRequired,
    changeDrawerState: React.PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 16 },
    text: { textAlign: 'left', marginBottom: 5 }
});

const mapStateToProps = (state) => ({ ...state.nav });
export default connect(mapStateToProps, { gotoScreen, changeDrawerState })(DrawerMenu);