import React, {Component} from 'react'
import { DrawerLayoutAndroid } from 'react-native'

export default class Layout extends Component {

    componentDidUpdate() {
        const {drawer} = this.refs;
        const {drawerOpen} = this.props;
        drawerOpen ? drawer.openDrawer() : drawer.closeDrawer();
    }

    render() {
        const {width, renderSideMenu, onDrawerClose, onDrawerOpen} = this.props;
        return (
            <DrawerLayoutAndroid
                ref='drawer'
                drawerWidth={width}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                onDrawerClose={onDrawerClose}
                onDrawerOpen={onDrawerOpen}
                renderNavigationView={renderSideMenu}
                >
                {this.props.children}
            </DrawerLayoutAndroid>
        )
    }
}

Layout.propTypes = {
    drawerOpen: React.PropTypes.bool.isRequired,
    width: React.PropTypes.number.isRequired,
    renderSideMenu: React.PropTypes.func.isRequired,
    children: React.PropTypes.element.isRequired,
    onDrawerClose: React.PropTypes.func.isRequired,
    onDrawerOpen: React.PropTypes.func.isRequired,
}