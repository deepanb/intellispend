'use strict';
import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Router, Scene, Modal} from 'react-native-router-flux';
import LoginScreen from './../../auth/components/login.js';
import CreatePinScreen from './../../auth/components/createPin.js';
import Home from './home.js';
import BudgetScreen from './../../budget/components/budget.js'
import PlanMonthlyBudgetScreen from './../../budget/components/planMonthlyBudget.js'
import AddBudget from './../../budget/components/addBudget.js'
import SplashScreen from './../../auth/components/splashScreen.js';
import {StatusBar} from 'react-native';
import {changeDrawerState, setTheme} from '../actions/navActions.js'
import {COLOR, Icon, IconToggle} from 'react-native-material-design';
import toolBarStyles from './../../common/toolBarStyles.js'
import ChooseCategory from './../../category/components/chooseCategory.js'
import SpendingScreen from './../../spends/components/spendings.js'

class App extends Component {

    constructor(props) {
        super(props);
        this.theme = 'googleGreen500';
    }

    getSceneStyle(/* NavigationSceneRendererProps */ props, computedProps) {
        const style = {
            flex: 1,
            backgroundColor: '#fff',
            shadowColor: null,
            shadowOffset: null,
            shadowOpacity: null,
            shadowRadius: null,
        };
        if (computedProps.isActive) {
            style.marginTop = computedProps.hideNavBar ? 0 : 56;
        }
        return style;
    }

    componentDidMount() {
        StatusBar.setBackgroundColor('#206a43', true); //a darker shade of medina green
        this.props.setTheme(this.theme);
    }

    renderNavBarIcon() {
        return (
            <IconToggle color={toolBarStyles.toolBarIconColor.color} onPress={() => this.props.changeDrawerState(true)}>
                <Icon name='menu' size={24} color={toolBarStyles.toolBarIconColor.color} style={toolBarStyles.toolBarLeftIcon} />
            </IconToggle>
        );
    }

    render() {
        const theme = this.props.theme || this.theme;
        return (
            <Router
                getSceneStyle={this.getSceneStyle}
                navigationBarStyle={[toolBarStyles.toolBar, { backgroundColor: COLOR[theme].color }]}
                titleStyle={toolBarStyles.navBarTitle}
                renderLeftButton={this.renderNavBarIcon.bind(this)}
                leftButtonIconStyle={toolBarStyles.backButtonStyle}
                >
                <Scene key="modal" component={Modal} >
                    <Scene key='root' hideNavBar='true'>
                        <Scene key="splash" type='replace' component={SplashScreen} initial={true} />
                        <Scene key="login" type='replace' component={LoginScreen} />
                        <Scene key="pin" type='replace' component={CreatePinScreen} />
                        <Scene key="home" type='replace' component={Home} >
                            <Scene key='main'>
                                <Scene key='budget' title='Budget' type='replace' component={BudgetScreen} duration={120} />
                                <Scene key='planMonthlyBudget' title='Plan'  component={PlanMonthlyBudgetScreen} duration={120} />
                                <Scene key='addBudget' title='Add'  component={AddBudget} duration={120} />
                                <Scene key='category' title='Choose Catgeory'  component={ChooseCategory} duration={120} />
                                <Scene key='spends' type='replace' title='Spendings'  component={SpendingScreen} duration={120} />
                            </Scene>
                        </Scene>
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

App.propTypes = {
    setTheme: React.PropTypes.func.isRequired,
    theme: React.PropTypes.string,
    changeDrawerState: React.PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({ theme: state.nav.theme });
export default connect(mapStateToProps, { changeDrawerState, setTheme })(App);