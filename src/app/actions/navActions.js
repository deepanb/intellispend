import {Actions} from 'react-native-router-flux'

//action creator for GOTO_SCREEN
export const gotoScreen = (screenName, title, screenProps) => {
    Actions[screenName].bind(Actions)(screenProps);
    return {
        type: 'GOTO_SCREEN',
        screenName,
        title
    }
};

//action creator for CHANGE_DRAWER_STATE
export const changeDrawerState = (drawerOpen) => (
    {
        type: 'CHANGE_DRAWER_STATE',
        drawerOpen
    }
);

//action creator for SET_THEME
export const setTheme = (theme) => (
    {
        type: 'SET_THEME',
        theme
    }
);