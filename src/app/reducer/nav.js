//reducer for nav actions
const nav = (state={}, action) => {
    switch (action.type) {
        case 'SET_THEME':
            return {
                ...state,
                theme: action.theme
            }
            
        case 'GOTO_SCREEN':
        {
            if(action.screenName === 'home')
                return {
                    ...state,
                    currentScreen: 'budget',
                    title: 'Budget'
                }
            else
                return {
                    ...state,
                    currentScreen: action.screenName,
                    title: action.title
            }
        }
        case 'CHANGE_DRAWER_STATE':
            return {
                ...state,
                open: action.drawerOpen
            }
        default:
            return state;
    }
}

export default nav;