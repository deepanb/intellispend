
import * as User from './user/userQueries.js';
import * as BudgetLine from './budgetLines/budgetLinesQueries.js';

const realmDal = {
    User,
    BudgetLine
}

export default realmDal
