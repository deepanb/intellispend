import Realm from 'realm'
import User from './user/user.js'
import BudgetLine from './budgetLines/budgetLines.js'

const intelliSpendRealm = new Realm({
    schema: [
        User,
        BudgetLine
    ]
});

export default intelliSpendRealm; 