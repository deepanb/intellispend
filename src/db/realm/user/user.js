import Realm from 'realm'
import SCHEMA from './../schemaNames.js'

export default class User extends Realm.Object {}
User.schema = {
    name: SCHEMA.USER,
    properties: {
        pin: 'string'
    }
}
