import intelliSpendRealm from './../intelliSpendRealm.js';
import SCHEMA from './../schemaNames.js'

export const getUserInfo = () => {
    let userInfo = {};
    let results = intelliSpendRealm.objects(SCHEMA.USER);
    if (results) {
        results.every(u => (userInfo.pin = u.pin));
    }
    return userInfo;
}

export const writeUserInfo = (hashedKey) => {
    intelliSpendRealm.write(() => { intelliSpendRealm.create(SCHEMA.USER, { pin: hashedKey }) });
}