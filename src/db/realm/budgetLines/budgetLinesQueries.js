import intelliSpendRealm from './../intelliSpendRealm.js'
import uuid from './../uuidGenerator.js'
import {monthNames} from './../../../common/presets.js'
import SCHEMA from './../schemaNames.js'
import _ from 'lodash'

export const fetchBudgetLines = () => {
    let budgetLines = [];
    const realmBudgetLines = intelliSpendRealm.objects(SCHEMA.BUDGET_LINE);
    if (realmBudgetLines) {
        budgetLines = realmBudgetLines.map(budgetLine => (
            _.pick(
                budgetLine,
                ['id', 'month', 'description', 'category', 'amount', 'repeats', 'repeatId']
            )));
    }
    return budgetLines;
}

export const deleteBudgetLines = (id) => {
    let deletedIds = [];
    let itemToDelete = intelliSpendRealm.objectForPrimaryKey(SCHEMA.BUDGET_LINE, id);
    const {repeats, repeatId} = itemToDelete;
    switch (repeats) {
        case 'Weekly':
        case 'Monthly':
            {
                intelliSpendRealm.write(() => {
                    let itemsToDelete = intelliSpendRealm.objects(SCHEMA.BUDGET_LINE).filtered('repeatId == $0', repeatId);
                    deletedIds = itemsToDelete.map(item => _.pick(item, ['id', 'month']));
                    intelliSpendRealm.delete(itemsToDelete); 
                })
                break;
            }

        case 'None':
        default:
            {
                intelliSpendRealm.write(() => {
                    deletedIds.push(_.pick(itemToDelete, ['id', 'month']));
                    intelliSpendRealm.delete(itemToDelete);
                })
            }
    }

    return deletedIds;
}

export const saveBudgetLines = ({id, month, description, category, amount, repeats, repeatId, mode}) => (

    mode === 'edit' ?
        editBudgetLines(id, month, description, category, amount, repeats, repeatId) :
        addBudgetLines(month, description, category, amount, repeats)
)

const editBudgetLines = (id, month, description, category, amount, repeats, repeatId) => {
    let budgetLines = {added : [], edited: [], deleted:[]};
    let old = intelliSpendRealm.objectForPrimaryKey(SCHEMA.BUDGET_LINE, id);
    if (old.repeats === 'None' && repeats === 'None') {
        budgetLines.edited.push({ id: old.id, month, description, category, amount, repeats, repeatId });
    }
    else if (old.repeats !== 'None' && repeats !== 'None') {
        let results = intelliSpendRealm.objects(SCHEMA.BUDGET_LINE).filtered('repeatId == $0', old.repeatId);
        budgetLines.edited = results.map((line) => ({ id: line.id, month: line.month, description, category, amount, repeats, repeatId }))
    }
    else if (old.repeats === 'None' && repeats !== 'None') {
        let itemToDelete = intelliSpendRealm.objectForPrimaryKey(SCHEMA.BUDGET_LINE, old.id);
        budgetLines.deleted.push(_.pick(itemToDelete, ['id', 'month']));
        let newRepeatId = uuid();
        budgetLines.added = monthNames.map((item) => ({ id: uuid(), month: item.month, description, category, amount, repeats, repeatId:newRepeatId }));
    }
    else if (old.repeats !== 'None' && repeats === 'None') {
        let itemsToDelete = intelliSpendRealm.objects(SCHEMA.BUDGET_LINE).filtered('repeatId == $0', old.repeatId);
        budgetLines.deleted = itemsToDelete.map(item => (_.pick(item, ['id', 'month'])));
        budgetLines.added.push({id: uuid(), month, description, category, amount, repeats:'None', repeatId:'-1'});
    }

    intelliSpendRealm.write(() => {
        budgetLines.deleted.forEach(itemToDelete => {
            let item = intelliSpendRealm.objectForPrimaryKey(SCHEMA.BUDGET_LINE, itemToDelete.id);
            intelliSpendRealm.delete(item);
        });
        budgetLines.added.forEach(line => intelliSpendRealm.create(SCHEMA.BUDGET_LINE, line));
        budgetLines.edited.forEach(line => intelliSpendRealm.create(SCHEMA.BUDGET_LINE, line, true));
    });

    return budgetLines;
}

const addBudgetLines = (month, description, category, amount, repeats) => {
    let budgetLines = [];
    switch (repeats) {
        case 'Weekly':
        case 'Monthly':
            {
                let repeatId = uuid();
                budgetLines = monthNames.map((item) => ({ id: uuid(), month: item.month, description, category, amount, repeats, repeatId }));
                break;
            }

        case 'None':
        default:
            budgetLines.push({ id: uuid(), category, month, description, amount, repeats, repeatId: '-1' });
    }

    intelliSpendRealm.write(() => {
        budgetLines.forEach((budgetLine) => intelliSpendRealm.create(SCHEMA.BUDGET_LINE, budgetLine));

    });

    return budgetLines;
}