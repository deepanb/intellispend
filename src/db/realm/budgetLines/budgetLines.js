import Realm from 'realm'
import SCHEMA from './../schemaNames.js'

export default class BudgetLine extends Realm.Object {}
BudgetLine.schema = {
    name: SCHEMA.BUDGET_LINE,
    primaryKey: 'id',
    properties: {
        id: 'string',
        month: 'string',
        description: 'string', 
        category: 'string', //TODO: convert this to json
        amount: {type: 'double'},
        repeats: { type: 'string', default: 'None' },
        repeatId: { type: 'string', default: '-1' }
    }
}